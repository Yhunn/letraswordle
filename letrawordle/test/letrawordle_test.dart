import 'package:letrawordle/letrawordle.dart';
import 'package:test/test.dart';

void main() {
  group('coincidencias de letras para juan', () {
    test('si es juno', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("juno");
      expect(coincidenciasExactas(m, i), inicializarLetras("ju").toSet());
    });
    test('si es jias', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("jias");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 2, letra: "a"));
      expect(coincidenciasExactas(m, i), x.toSet());
    });
    test('si es joen', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("joen");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 3, letra: "n"));
      expect(coincidenciasExactas(m, i), x.toSet());
    });
  });
  group('coincidencias no exactas para juan', () {
    test('si es jona', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("jona");
      List<Prueba> x = [];
      x.add(Prueba(numero: 3, letra: "n"));
      x.add(Prueba(numero: 2, letra: "a"));
      expect(coincidenciasNoExactas(m, i, coincidenciasExactas(m, i)), x);
    });
    test('si es aaron', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("aaron");
      List<Prueba> x = [];
      x.add(Prueba(numero: 2, letra: "a"));
      x.add(Prueba(numero: 3, letra: "n"));
      expect(coincidenciasNoExactas(m, i, coincidenciasExactas(m, i)), x);
    });
  });
  group('ninguna coincidencia para juan', () {
    test('si es jule', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("jule");
      List<Prueba> x = [];
      x.add(Prueba(numero: 2, letra: "l"));
      x.add(Prueba(numero: 3, letra: "e"));
      expect(noCoincidencia(m, i), x.toSet());
    });
    test('si es robn', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("robn");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "r"));
      x.add(Prueba(numero: 1, letra: "o"));
      x.add(Prueba(numero: 2, letra: "b"));
      expect(noCoincidencia(m, i), x.toSet());
    });
    test('si es real', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("real");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "r"));
      x.add(Prueba(numero: 1, letra: "e"));
      x.add(Prueba(numero: 3, letra: "l"));
      expect(noCoincidencia(m, i), x.toSet());
    });
    test('si es asor', () {
      var m = inicializarLetras("juan");
      var i = inicializarLetras("asor");
      List<Prueba> x = [];
      x.add(Prueba(numero: 1, letra: "s"));
      x.add(Prueba(numero: 2, letra: "o"));
      x.add(Prueba(numero: 3, letra: "r"));
      expect(noCoincidencia(m, i), x.toSet());
    });
  });
}
