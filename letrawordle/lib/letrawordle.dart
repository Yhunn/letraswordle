void main() {
  var cmp = inicializarLetras("juan");
  var cip = inicializarLetras("juno");

  var coin = coincidenciasExactas(cmp, cip);

  List<Prueba> coinN = coincidenciasNoExactas(cmp, cip, coin);

  Set<Prueba> noCoin = noCoincidencia(cmp, cip);

  print('Coincidencias exactas');
  print(coin);

  print('Coincidencias no exactas');
  print(coinN);
  // faltan las que no coinciden
  print('Ninguna coincidencia');
  print(noCoin);
}

class Prueba {
  final int numero;
  final String letra;

  const Prueba({required this.numero, required this.letra});
  @override
  bool operator ==(other) =>
      other is Prueba && numero == other.numero && letra == other.letra;
  @override
  int get hashCode => numero.hashCode ^ letra.hashCode;
  @override
  toString() => 'P($numero:$letra)';
}

Set<Prueba> coincidenciasExactas(Iterable<Prueba> m, Iterable<Prueba> i) {
  return m.toSet().intersection(i.toSet());
}

Iterable<Prueba> inicializarLetras(String m) {
  var ml = m.split('').asMap();
  var cm = ml.entries;
  return cm.map((e) => Prueba(letra: e.value, numero: e.key));
}

List<Prueba> coincidenciasNoExactas(
    Iterable<Prueba> m, Iterable<Prueba> i, Set<Prueba> coin) {
  List<Prueba> coinN = [];
  for (var elemento in i) {
    coinN.addAll(m.where((e) {
      print(e);
      return e.letra == elemento.letra && !coin.contains(e);
    }));
  }
  return coinN.toSet().toList();
}

Set<Prueba> noCoincidencia(Iterable<Prueba> m, Iterable<Prueba> i) {
  List<Prueba> noCoin = [];
  for (var elemento in m) {
    noCoin.addAll(i.where((e) {
      return elemento != e && !m.contains(e);
    }));
  }
  return noCoin.toSet();
}
